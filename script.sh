#!/bin/bash


# Copier le fichier de configuration sur le serveur

cp ./config__files/ssh/sshd_config /etc/ssh/sshd_config

cp ./config__files/ssh/Banner /etc/Banner

printf "enter a username :\n"

read username

adduser $username

usermod -aG sudo $username

printf "enter a password :\n"

passwd $username

printf "enter a you public key :\n"

mkdir /home/$username/.ssh/

read KeySsh

echo $KeySsh >> /home/$username/.ssh/authorized_keys

# Redemarrer le service sshd 

systemctl restart sshd
